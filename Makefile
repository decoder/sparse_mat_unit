CXX       = g++
RM        = rm -f

CPPFLAGS  = -I.
CXXFLAGS  = -Wall -Werror -g3 -O0
LDFLAGS   =
LIBS      =

FPRODS    =
BUILD     = build

SRCS      = main.cpp matrix_sparse.cpp

OBJS      = $(patsubst %.cpp, $(BUILD)/%.o, $(SRCS))
DEPS      = $(patsubst %.o,%.d,$(OBJS))

EXE       = sparse_mat_unit

.PHONY: all
all: $(EXE)

FPRODS += $(EXE)
$(EXE): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

$(OBJS): $(BUILD)/%.o: %.cpp
	@mkdir -p `dirname $@`
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MMD -MP -o $@ -c $<

.PHONY: clean
clean:
	$(RM) -R $(BUILD)
	$(RM) $(FPRODS)

-include $(DEPS)
